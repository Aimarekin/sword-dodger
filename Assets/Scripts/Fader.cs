﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fader : MonoBehaviour {

    public Image Fade;

	// Use this for initialization
	void Start () {
        Fade.DOColor(new Color(0, 0, 0, 0), 0.5f);
    }

    public IEnumerator RestartScene()
    {
        Fade.DOColor(new Color(0, 0, 0, 1), 0.5f);
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("MainGame");
    }
}
