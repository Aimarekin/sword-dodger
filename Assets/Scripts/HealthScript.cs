﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class HealthScript : MonoBehaviour {


    public SpriteRenderer sprRend;
    public bool canDamage = true;
    public int health = 4;
    private int round = 0;
    public Animator anim;
    public RoundController roundScr;
    public MovementController moveScr;
    public Rigidbody2D rb_2d;
    private AudioSource mainMusic;
    private AudioSource damageSound;
    private AudioSource gameOverMusic;
    private AudioSource gameOverSound;
    public Transform gameOverTrans;
    public RectTransform roundText;
    public TextMeshProUGUI extraLifes;
    private int hi_score = 0;
    public Image Fade;
    public Canvas GameOver;
    public TextMeshProUGUI hiScoreText;
    private bool canRestart = false;
    public TextMeshProUGUI gameOverRoundText;
    public Fader FaderScript;

    // Hearts
    public SpriteRenderer[] hearts;

    // Get an array of digits in a number
    public int[] NumbersIn(int value)
    {
        var numbers = new Stack<int>();

        for (; value > 0; value /= 10)
            numbers.Push(value % 10);

        return numbers.ToArray();
    }

    // Update hearts
    public void updateHearts(int quantity)
    {

        // Ensure there are no hearts blinking
        StopCoroutine("BlinkHeart");

        health += quantity;

        // Blink a heart if neccessary
        if (quantity > 0 && health <= 4)
        {
            StartCoroutine("BlinkHeart");
        }

        if (health >= 1)
        {
            hearts[0].enabled = true;
        }
        else
        {
            hearts[0].enabled = false;
        }

        if (health >= 2)
        {
            hearts[1].enabled = true;
        }
        else
        {
            hearts[1].enabled = false;
        }

        if (health >= 3)
        {
            hearts[2].enabled = true;
        }
        else
        {
            hearts[2].enabled = false;
        }

        if (health >= 4)
        {
            hearts[3].enabled = true;
        }
        else
        {
            hearts[3].enabled = false;
        }

        if (health > 4) {
            string lifesText = "+  ";

            foreach (int digit in NumbersIn(health - 4))
            {
                lifesText += "<sprite=" + digit + ">";
            }

            extraLifes.text = lifesText;
        }
        else
        {
            extraLifes.text = "";
        }
    }

    // Use this for initialization
    void Start () {
        // Store the AudioSources
        AudioSource[] AudioComponents = GetComponents<AudioSource>();

        // Get the DamageSound to play it
        damageSound = AudioComponents[1];

        // Get the gameOverMusic to play it
        gameOverMusic = AudioComponents[3];

        // Get the gameOverSound to play it
        gameOverSound = AudioComponents[4];

        // Get the mainMusic to stop it
        mainMusic = AudioComponents[0];

        // Init the hi-scores
        bool has_hi_scores = PlayerPrefs.HasKey("hi_score");
        if (has_hi_scores)
        {
            hi_score = PlayerPrefs.GetInt("hi_score");
        }
        else
        {
            hi_score = 0;
            PlayerPrefs.SetInt("hi_score", 0);
        }
    }
	
    IEnumerator AvoidDamage()
    {
        canDamage = false;
        anim.SetTrigger("Damaged");
        yield return new WaitForSeconds(0.2f);
        StartCoroutine("BlinkCharacter");
        yield return new WaitForSeconds(2.3f);
        canDamage = true;
        StopCoroutine("BlinkCharacter");
        sprRend.enabled = true;
    }

    IEnumerator BlinkCharacter()
    {
        while (true)
        {
            sprRend.enabled = false;
            yield return new WaitForSeconds(0.1f);
            sprRend.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    public IEnumerator BlinkHeart()
    {

        SpriteRenderer heart = hearts[health - 1];

        for (int i = 0; i < 3; i++)
        {
            heart.enabled = true;
            yield return new WaitForSeconds(0.5f);
            heart.enabled = false;
            yield return new WaitForSeconds(0.5f);
        }
        heart.enabled = true;

    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Space) && canRestart)
        {
            StartCoroutine(FaderScript.RestartScene());
            canRestart = false;

        }
    }

    IEnumerator die (Rigidbody2D sword_rb2d)
    {
        yield return new WaitForSeconds(2);
        sword_rb2d.gravityScale = 1.75f;
        yield return new WaitForSeconds(0.5f);
        anim.SetBool("Dead", true);
        gameOverSound.Play();
        yield return new WaitForSeconds(1.2f);
        gameOverTrans.DOScale(new Vector3(0.75f, 0.75f), 0.5f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(1.75f);
        Fade.DOColor(new Color(0, 0, 0, 0.48f), 1);
        gameOverMusic.Play();
        yield return new WaitForSeconds(0.75f);

        // Change the text
        string text = "";
        foreach (int digit in NumbersIn(round))
        {
            text += "<sprite=" + digit + ">";
        }
        gameOverRoundText.text = text;


        // Set the hi-score text
        if (hi_score == 0)
        {
            hiScoreText.text = "Se ha registrado tu record.\n\n¡Intenta batirlo!";
            hi_score = round;
            PlayerPrefs.SetInt("hi_score", round);
        }
        else
        {
            if (hi_score > round)
            {
                hiScoreText.text = "Tu record es " + hi_score + ".\n\n¡Intenta batirlo!";
            }
            else
            {
                if (hi_score == round)
                {
                    hiScoreText.text = "Tu record es " + hi_score + ".\n\n¡Por poco!";
                }
                else
                {
                    if (hi_score < round)
                    {
                        hiScoreText.text = "Tu record era " + hi_score + ".\n\n¡Lo has batido!";
                        hi_score = round;
                        PlayerPrefs.SetInt("hi_score", round);
                    }
                }
            }
        }

        GameOver.enabled = true;
        canRestart = true;

    }

    // Detect when a sword touches the player
    void OnTriggerEnter2D(Collider2D collider)
    {
        //Debug.Log("Collision!");

        // Check if we collided with a sword and if we can get damage
        if (collider.gameObject.tag == "Sword" && canDamage)
        {
            damageSound.Play();
            updateHearts(-1);

            if (health <= 0)
            {
                Debug.Log("Hell yeah u died");

                // Avoid getting more damage
                canDamage = false;

                // Store how much we survived
                round = roundScr.Round;

                // Avoid other scripts pretend we're alive.
                // Duh, you're dead.
                Destroy(roundScr);
                Destroy(moveScr);

                // Stop the main music
                mainMusic.Stop();

                // Stop the player from moving. We got him kidnapped.
                rb_2d.velocity = new Vector2(0, 0);

                // Make the animator stop the player
                anim.SetFloat("Speed", 0);

                // Paralize the sword that hit you
                Rigidbody2D collider_rb2d = collider.gameObject.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
                collider_rb2d.velocity = new Vector2(0, 0);

                // Destroy all other swords
                GameObject[] swords = GameObject.FindGameObjectsWithTag("Sword");
                foreach (GameObject sword in swords)
                {
                    if (sword != collider.gameObject)
                    {
                        Destroy(sword);
                    }
                }

                // Move the round text so the game over text doesn't cover it.
                roundText.DOLocalMoveY(-120, 1);

                // Die.
                StartCoroutine(die(collider_rb2d));
            }
            else
            {
                // Make the player blink
                StartCoroutine("AvoidDamage");
            }
        }

    }
}
