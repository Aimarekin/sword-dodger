﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour
{

    // This script controls the movement of the character
    
    public float speed;              // Variable to store the player's movement speed.

    private Rigidbody2D rb_2d;       // Quick reference to the Rigidbody2D

    private RectTransform re_trans;  // We'll later mirror the sprite

    private Animator anim;           // We need to inform the Animator of the axises' max speed

    void Start()
    {
        // Quick reference to the RigidBody2D
        rb_2d = GetComponent<Rigidbody2D>();

        // We'll later mirror the sprite
        re_trans = GetComponent<RectTransform>();

        // We need to inform the Animator of the axises' max speed
        anim = GetComponent<Animator>();

    }

    void FixedUpdate()
    {
        // Store the horizontal movement
        float HorizontalMovement = Input.GetAxis("Horizontal");

        // Store the vertical movement
        float VerticalMovement = Input.GetAxis("Vertical");


        // Mirror or not the object depending on at which side it is walking to
        if (HorizontalMovement > 0.01)
        {
            // Debug.Log("Greater");
            re_trans.localScale = new Vector3(1,1,1);
        }
        else
        {
            if (HorizontalMovement < -0.01)
            {
                // Debug.Log("Lesser");
                re_trans.localScale = new Vector3(-1, 1, 1);
            }
        }

        // Inform the Animator of the axises' speed so he can apply the movement animation.
        // We'll tell him of the biggest speed to cover all movement directions.
        anim.SetFloat("Speed", Mathf.Max(Mathf.Abs(HorizontalMovement), Mathf.Abs(VerticalMovement)));


        // Combine the horizontal and vertical movement into a Vector2
        Vector2 movement = new Vector2(HorizontalMovement, VerticalMovement);

        // Here goes the magic! The Vector2 is set as the velocity of the Rigidbody2D
        rb_2d.velocity = movement * (speed / 10);
    }
}