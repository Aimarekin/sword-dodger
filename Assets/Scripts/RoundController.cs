﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class RoundController : MonoBehaviour {
    
    public int Round = 1;
    public int ToLife = 4;
    public bool Life = false;
    public GameObject Sword;
    private Transform trans;
    private AudioSource oneUpSound;
    private AudioSource mainMusic;
    public GameObject oneUpSprite;
    private Transform oneUpTransform;
    public HealthScript H_scr;
    public TextMeshProUGUI roundText;
    public TextMeshProUGUI lifeText;

    // Convert an int into an array
    public int[] NumbersIn(int value)
    {
        var numbers = new Stack<int>();

        for (; value > 0; value /= 10)
            numbers.Push(value % 10);

        return numbers.ToArray();
    }

    // Wait 3 seconds, tween the 1UP text, wait for the one up sound to finish and then unmute the main music
    IEnumerator WaitAudio()
    {
        yield return new WaitForSeconds(2.5f);
        // Debug.Log("3 seconds past!");
        yield return new WaitWhile(() => oneUpSound.isPlaying);
        // Debug.Log("Sound finished!");
        mainMusic.mute = false;
    }


    IEnumerator oneUpTween()
    {
        yield return new WaitForSeconds(1.5f);
        oneUpTransform.DOScale(new Vector3(0f, 0f), 0.75f).SetEase(Ease.InElastic);
    }

    // Go to the next round every 10 seconds and grant an extra life every 5 rounds
    void ManageRounds()
    {
        Round += 1;

        // Change the text
        string text = "";
        foreach (int digit in NumbersIn(Round) )
        {
            text += "<sprite="+digit+">";
        }
        roundText.text = text;

        ToLife -= 1;
        if (ToLife == 0)
        {
            lifeText.text = "(¡VIDA!)";
            Life = true;
            ToLife = 5;

            // Grant the life
            H_scr.updateHearts(1);

            mainMusic.mute = true;
            oneUpSound.Play();
            StartCoroutine("WaitAudio");
            oneUpTransform.DOScale(new Vector3(0.25f, 0.25f), 0.75f).SetEase(Ease.OutElastic);
            StartCoroutine("oneUpTween");

        }
        else
        {
            lifeText.text = "("+ToLife+" para ¡VIDA!)";
        }

        // Debug.Log("10 seconds!");
    }


    // Throw swords against the character, which it must dodge
    void ThrowSwords()
    {

        // How many swords to throw. It will be the round amount minus a random of 0 to 5. Will always be at least one.
        float swordAmount = Round - Mathf.Round(Random.Range(0, Mathf.Min(5, Round - 1)));


        // Throw as much swords as we just decided
        for (int i = 0; i < swordAmount; i++)
        {
            float posX;
            float posY;

            // If random decides 0, then decide side and make up-down free
            if (Mathf.Round(Random.Range(0f, 1f)) == 0) {
                if (Mathf.Round(Random.Range(0f, 1f)) == 0)
                {
                    posX = -2.5f;
                }
                else
                {
                    posX = 2.5f;
                }
                posY = Random.Range(-1.5f, 1.5f);
            }
            // Else decide up-down and make side free
            else {
                if (Mathf.Round(Random.Range(0f, 1f)) == 0)
                {
                    posY = -1.5f;
                }
                else
                {
                    posY = 1.5f;
                }
                posX = Random.Range(-2.5f, 2.5f);
            }

            // Make a Vector3 from these random decisions. It will be the sword's position.
            Vector3 pos = new Vector3(posX, posY, -2);

            // Get a Quaternion that is looking at the character
            Quaternion looking = Quaternion.FromToRotation(pos, trans.position);
            looking.y = 0f;
            looking.x = 0f;

            // Make the sword
            GameObject instance = Instantiate(Sword, pos, looking);

            // Get the Rigibody2D as we will later need to apply a force
            Rigidbody2D ins_rb2d = instance.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;

            // Get the Transform as we will later need to get the position
            Transform ins_tr = instance.GetComponent(typeof(Transform)) as Transform;
            
            // Get the force to apply to aim correctly
            Vector2 force = trans.position - ins_tr.position;

            // Apply a force so the sword aims for the character, with a random offset
            ins_rb2d.velocity = (force.normalized * 1.25f) +
                new Vector2(Random.Range(-1, 1) / 10, Random.Range(-1, 1) / 10);
                // The offset can be a min of (-0.1, -0.1) and a max of (0.1, 0.1)

            //Debug.Log(force.normalized);

        }
    }

	void Start () {

        // Go to the next round every 10 seconds and throw swords every 2.

        InvokeRepeating("ManageRounds", 10f, 10f);
        InvokeRepeating("ThrowSwords", 2f, 2f);

        // Store the AudioSources
        AudioSource[] AudioComponents = GetComponents<AudioSource>();

        // Get the 1UP sound to play it
        oneUpSound = AudioComponents[2];

        // and the main music to mute it
        mainMusic = AudioComponents[0];

        // Play the music to avoid latency
        mainMusic.Play();

        // Get the Transform component of this
        trans = GetComponent<Transform>();

        // Get the Transform component of the oneUpSprite to Tween it
        oneUpTransform = oneUpSprite.GetComponent(typeof(Transform)) as Transform;
	}

}
