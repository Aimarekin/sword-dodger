﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscordController : MonoBehaviour {

    public DiscordRpc.RichPresence presence = new DiscordRpc.RichPresence();
    public DiscordRpc.EventHandlers handler = new DiscordRpc.EventHandlers();

    public void ready(ref DiscordRpc.DiscordUser connectedUser)
    {
        Debug.Log("Ready");
        presence.details = "In menu";
        presence.smallImageKey = "";
        DiscordRpc.UpdatePresence(presence);
        Debug.Log("Sent");
    }

    public void error (int errorCode, string message)
    {
        Debug.LogError("Unhandled Discord Rpc error: Error code " + errorCode + " Message: " + message);
    }

    public void disconnect(int errorCode, string message)
    {
        Debug.LogError("Unhandled Discord Rpc error: Error code " + errorCode + " Message: " + message);
    }

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(gameObject);
        handler.readyCallback = ready;
        handler.errorCallback = error;
        handler.disconnectedCallback = disconnect;
        presence.largeImageKey = "sword";
        presence.largeImageText = "Sword Dodger";
        DiscordRpc.Initialize("523899524968611850", ref handler, true, "");
        Debug.Log("Init");
	}
	
	// Update is called once per frame
	void Update () {
        DiscordRpc.RunCallbacks();

    }
}
