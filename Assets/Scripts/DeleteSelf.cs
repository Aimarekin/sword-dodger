﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteSelf : MonoBehaviour {

    private Transform trans;

    public float runAfter;
    public float runEvery;
    public float maxX;
    public bool checkX;
    public float maxY;
    public bool checkY;

    void Start()
    {

        trans = GetComponent<Transform>();
        InvokeRepeating("Check", runAfter, runEvery);

    }


    void Check () {

        if (Mathf.Abs(trans.position.x) > maxX && checkX)
        {
            Destroy(gameObject);
        }
        if (Mathf.Abs(trans.position.y) > maxY && checkY)
        {
            Destroy(gameObject);
        }

    }

}
