﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour {

    public GameObject Cloud;

	// Spawn a cloud
	void SpawnCloud () {

        float posY;
        float posX;
        bool isLeft;

        posY = Random.Range(-1f, 1f);

        // Decide side
        if (Mathf.Round(Random.Range(0f, 1f)) == 0)
        {
            posX = -2.75f;
            isLeft = true;
        }
        else
        {
            posX = 2.75f;
            isLeft = false;
        }

        // Decide position
        Vector3 pos = new Vector3(posX, posY, 6);

        // Make the cloud
        GameObject instance = Instantiate(Cloud, pos, new Quaternion(0,0,0,0));

        // Get the Transform as we will later need to choose size
        Transform tr = instance.GetComponent(typeof(Transform)) as Transform;

        // Choose size
        float size = Random.Range(4f, 6f);

        // Set size
        tr.localScale = new Vector3(size, size);

        // Get the Rigibody2D as we will later need to apply a force
        Rigidbody2D rb_2d = instance.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;

        // Apply forces
        if (isLeft)
        {
            rb_2d.velocity = new Vector2(0.5f, 0);
        }
        else
        {
            rb_2d.velocity = new Vector2(-0.5f, 0);
        }

    }

    void Start()
    {
        InvokeRepeating("SpawnCloud", 0f, 5f);
    }
}
