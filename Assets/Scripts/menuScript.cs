﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class menuScript : MonoBehaviour {

    public GameObject sword;
    public Transform mainCamera;
    public Camera cameraComponent;
    public AudioSource audioComponent;

	// Use this for initialization
	void Start () {
        InvokeRepeating("CreateSword", 0, 0.25f);
	}
	
	
	void CreateSword()
    {
        GameObject instance = Instantiate(sword, new Vector3(Random.Range(-2.75f, 2.75f), 2, 7), new Quaternion(Random.Range(0,360), Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
        Rigidbody2D rb = instance.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        rb.simulated = true;
    }

    void Update()
    {
        // Go to game if space was pressed
        if (Input.GetKeyUp(KeyCode.Space))
        {
            StartCoroutine("StartGame");
        }
    }

    IEnumerator StartGame()
    {
        mainCamera.DOMoveZ(10, 2).SetEase(Ease.OutCubic);
        cameraComponent.DOColor(new Color(0, 0, 0), 2);
        audioComponent.DOFade(0, 2);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("MainGame");
    }
}

